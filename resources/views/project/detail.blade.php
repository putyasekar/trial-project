@include('base.header')

<div class="content-wrapper">
  <section class ="content-header">
    <h1>Event</h1>
  </section>

  <section class="content">

     <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Detail </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form role="form" action="/project/{{$Project->id}}/update" method="POST" enctype="multipart/form-data">
                @csrf

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <table class="table table-bordered">
                  <tr>
                    <td>ID</td>
                    <td>{{ $Project->id }}</td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td>{{ $Project->name }}</td>
                  </tr>
                  <tr>
                   <td>Category</td>
                   <td>{{ $Project->category['name'] }}</td>
                 </tr>
                 <tr>
                   <td>Image</td>
                   <td><img src="/images1/{{ $Project->image1 }}" style="width: 150px; height: 150px"></td>
                 </tr>
                  <tr>
                   <td>Description</td>
                   <td>{{ $Project->description }}</td>
                 </tr>
               
                </table>
                <div class="form-data">
                  <!-- <input class="btn btn-primary" type="submit" value="update"></input> -->
                  <a class="btn btn-warning" href="/project">Back</a>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
      </div>
  </section>
</div>

@include('base.footer')