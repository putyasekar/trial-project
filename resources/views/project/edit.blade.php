@include('base.header')

<div class="content-wrapper">
  <section class="content-header">
    <h1>Event</h1>
  </section>


  <section class="content">

    <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Event</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form role="form" action="/project/{{$Project->id}}/update" method="POST" enctype="multipart/form-data">
                @csrf

                @if (count($errors) > 0)
                 <div class="alert alert-danger">
                     <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                     </ul>
                 </div>
               @endif
                <!-- text input -->
                <div class="form-group">
                  <label >Name</label>
                  <input type="text" class="form-control" name="name" value="{{ $Project->name }}">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail">Category</label>
                  <select class="form-control" name="category_id" required>
                    @foreach($Category as $category)
                    <option value="{{ $category->id }}" {{($Project->category_id)?'selected':'' }}>{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label >Picture</label>
                  <input type="file" name="image1" class="form-control" value="{{ $Project->image }}">
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <input class="form-control" name="description" value="{{ $Project->description }}">
                </div>
              
                <div class="form-group">
                  <input class="btn btn-primary" type="submit" value="update"></input>
                  <a class="btn btn-warning" href="/project">Back</a>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
  </section>
</div>


@include('base.footer')