@include('base.header')

<div class="content-wrapper">
 <section class="content-header">
   <h1>Event</h1>
 </section>

 <section class="content">
     <div class="row">
       <!-- left column -->
       <div class="col-md-12">
         <!-- general form elements -->
         <div class="box box-primary">
           <div class="box-header with-border">
             <h3 class="box-title">Add Event</h3>
           </div>
           <!-- /.box-header -->
           <!-- form start -->
           @if(Session::has('message'))
           <h4><strong>{{session::get('message')}}</strong></h4>
           @endif
           <form action="/project/store" method="post" enctype="multipart/form-data">

             @csrf

             @if (count($errors) > 0)
             <div class="alert alert-danger">
               <ul>
                 @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
                 @endforeach
               </ul>
             </div>

             @endif

             <div class="box-body">
               <div class="form-group">
                 <label> Name </label>
                 <input class="form-control" name="name" placeholder="Enter Name">
               </div>
             
               <div class="form-group">
                  <label for="exampleInputEmail">Category</label>
                  <select class="form-control" name="category_id" required>
                    <option>-- Pilih Salah Satu --</option>
                    @foreach($Category as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label >Image</label>
                  <input type="file" name="image1" class="form-control">
                </div>
                <div class="form-group">
                  <label>Description</label>
                  <input class="form-control" name="description" placeholder="Enter Description">
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Event Table</h3>

              <div class="box-tools">
                    <form action="{{ route('search_project') }}" method="GET" class="form-group">
                      <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-default">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>

            </form>
            </div>
            <form action="/project" method="GET">
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Category</th>
                  <th>Image</th>
                  <th>Desription</th>
              
                </tr>
                @foreach($Project as $item)
                <tr>
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->category_id }}</td>
                  <td><img src="/images1/{{ $item->image1 }}" style="width: 50px; height: 40px"></td>
                  <td>{{ $item->description }}</td>
            
                  <td>
                    <a class="btn btn-success btn-sm" href="/project/{{$item->id}}/edit">Edit</a>
                    <a class="btn btn-danger btn-sm" href="/project/{{$item->id}}/delete">Delete</a>
                    <a class="btn btn-primary btn-sm" href="/project/{{$item->id}}">Detail</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <div class="text-center">
              {{ $Project->links() }}
            </div>
          </form>
          </div>
        </div>
      </div>
    </section>

</div>

@include('base.footer')