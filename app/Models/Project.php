<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'table_project';

    public function Category()
    {
    	return $this->hasOne(Category::class, "id", "category_id");
    }
}