<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Category;
use Session;

class ProjectController extends Controller
{
    public function index()
    {
        $Category = Category::all();
        $Project = Project::orderBy('created_at','desc')->paginate(5);
        return view('project.index', compact('Project', 'Category'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'category_id' => 'required',
            'image1' => 'required |image|mimes:jpg,jpeg,png,gif',
            'description' => 'required',
           
        ]);

        try {
            $imageName1 = time().'.'.request()->image1->getClientOriginalExtension();
            request()->image1->move(public_path('images1'), $imageName1);
            
            $Project                = new Project();
            $Project->name          = $request->name;
            $Project->category_id   = $request->category_id;
            $Project->image1        = $imageName1;
            $Project->description   = $request->description;

            $Project->save();

            Session::flash('message', 'Data Berhasil Disimpan');
            return redirect()->back();
        } catch (Exception $e) {
            Session::flash('message', 'Data Gagal Disimpan');
            return redirect()->back();
        }
    }
    public function search_project(Request $request)
    {
        $Category = Category::query();
        $Project = Project::query();

        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $Project->where("name", "like", "%" . request()->query("search") . "%");
        }

        $pagination = 5;
        $Project = $Project->orderBy('created_at', 'desc')->paginate($pagination);

        return view('project.index', compact('Project', 'Category'));
    }
    public function edit($id)
    {
        $Category = Category::all();
        $Project = Project::find($id);

        return view('project.edit', compact('Project', 'Category'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'category_id' => 'required',
            'image1' => 'required |image|mimes:jpg,jpeg,png,gif',
            'description' => 'required',
           
        ]);
        
        $imageName1 = time().'.'.request()->image1->getClientOriginalExtension();
            request()->image1->move(public_path('images1'), $imageName1);
            
        $Category = Category::all();
        $Project = Project::find($id);
        $Project->name = $request->name;
        $Project->category_id = $request->category_id;
        $Project->image1 = $imageName1;
        $Project->description = $request->description;
     
        $Project->save();

        Session::flash('message','Berhasil update');
        return redirect()->back();
    }
    public function delete($id)
    {
        $Project = Project::find($id);

        $Project->delete();

        Session::flash('message','Berhasil menghapus');

        return redirect()->back();
    }
    public function detail($id)
    {
        $Category = Category::all();
        $Project = Project::find($id);

        return view('project.detail', compact('Project', 'Category'));
    }
}
