<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Session;

class CategoryController extends Controller
{
    public function index()
    {
       $Category = Category::orderBy('created_at', 'desc')->paginate(5);
        return view('category.index', compact('Category'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required'
    	]);

    	try{
    		$Category = new Category();
    		$Category->name = $request->name;
    		$Category->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e){
    		Session::flash('message', 'Data Tidak Berhasil Disimpan');
    		return redirect()->back();
    	}
    }
    public function search_category(Request $request)
    {
        $Category = Category::query();
        
        if (request()->has("search") && strlen(request()->query("search")) >= 1) {
            $Category->where("name", "like", "%" . request()->query("search") . "%");
        }

        $pagination = 5;
        $Category = $Category->orderBy('created_at', 'desc')->paginate($pagination);

        return view('category.index', compact('Category'));
    }
    public function edit($id)
    {
        $Category = Category::find($id);

        return view('category.edit', compact('Category'));
    }

    public function update(Request $request, $id)
    {
        $Category = Category::find($id);
        $Category->name = $request->name;
        
        $Category->save();

        Session::flash('message','Berhasil update');

        return redirect()->back();
    }

    public function delete($id)
    {
        $Category = Category::find($id);

        $Category->delete();

        Session::flash('message','Berhasil menghapus');

        return redirect()->back();
    }
}