<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});

//event
// Route::get('/project', 'ProjectController@index');
// Route::get('{id}/edit','ProjectController@edit');
// Route::post('{id}/update','ProjectController@update');
// Route::get('{id}/delete','ProjectController@delete');
// // Route::get('{id}', 'ProjectController@detail');
// Route::get('/search_project', 'ProjectController@search_project')->name('search_project');


Route::get('/project', 'ProjectController@index');
Route::post('project/store', 'ProjectController@store')->name('project.store');
Route::get('/search_project', 'ProjectController@search_project')->name('search_project');
Route::get('/project/{id}/delete','ProjectController@delete');
Route::get('/project/{id}/edit', 'ProjectController@edit');
Route::post('/project/{id}/update', 'ProjectController@update');
Route::get('/project/{id}','ProjectController@detail');


//category
// Route::post('/category/store', 'CategoryController@store')->name('category.store');
// Route::get('/search_category', 'CategoryController@search_category')->name('search_category');
// Route::get('/filter_category', 'CategoryController@filter_category')->name('filter_category');
// Route::get('{id}/edit','CategoryController@edit');
// Route::post('{id}/update','CategoryController@update');
// Route::get('{id}/delete','CategoryController@delete');
// Route::get('/category', 'CategoryController@index');
// // Route::get('{id}', 'CategoryController@detail');

Route::get('/category', 'CategoryController@index');
Route::post('category/store', 'CategoryController@store')->name('category.store');
Route::get('/search_category', 'CategoryController@search_category')->name('search_category');
Route::get('/category/{id}/delete','CategoryController@delete');
Route::get('/category/{id}/edit', 'CategoryController@edit');
Route::post('/category/{id}/update', 'CategoryController@update');